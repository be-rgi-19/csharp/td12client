﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TD12Client
{
    class Program
    {
        static void Main(string[] args)
        {
            RefServHm.Temps t1 = new RefServHm.Temps();
            RefServHm.Temps t2 = new RefServHm.Temps();
            RefServHm.Temps res;
       
            try
            {
                Console.WriteLine("Saisir le nombre d'heures du premier temps");
                t1.H = int.Parse(Console.ReadLine());
                Console.WriteLine("Saisir le nombre de minutes du premier temps");
                t1.M = int.Parse(Console.ReadLine());
                Console.WriteLine("Saisir le nombre d'heures du second temps");
                t2.H = int.Parse(Console.ReadLine()); 
                Console.WriteLine("Saisir le nombre de minutes du second temps");
                t2.M = int.Parse(Console.ReadLine()); 
                RefServHm.ServAddClient client = new RefServHm.ServAddClient();

                res = client.AddTemps(t1, t2);
                client.Close();

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                Console.ReadKey();
                throw;
            }
            Console.WriteLine("Resultat : {0}:{1}", res.H, res.M);
            Console.ReadKey();
        }
    }
}
